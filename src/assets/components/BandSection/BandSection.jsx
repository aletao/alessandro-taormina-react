export default function BandSection() {
  return (
    <>
      <div className="bandSection">
        <h2 id="band">The band</h2>
        <br />
        <span className="italic">We love music!</span>
        <p className="bandDescription">
          We have created a fictional band, what else to say? Lorem ipsum dolor
          sit amet, consectetur adipisci elit, sed do eiusmod tempor incidunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum
          exercitationem ullamco laboriosam, nisi ut aliquid ex ea commodi
          consequatur. Duis aute irure reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat
          cupiditat non proident, sunt in culpa qui officia deserunt mollit anim
          id est laborum
        </p>
        <div className="membersSection">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-4">
                <div
                  className="card"
                  style={{ width: "18rem", border: "none" }}
                >
                  <a href="">
                    <img
                      src="/guitarist.jpg"
                      className="card-img-top rounded-circle img-fluid"
                      alt="guitarist jimi hendrix"
                    />
                  </a>
                  <div className="card-body">
                    <h3>JIMI HENDRIX</h3>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div
                  className="card"
                  style={{ width: "18rem", border: "none" }}
                >
                  <a href="">
                    <img
                      src="/guitarist.jpg"
                      className="card-img-top rounded-circle img-fluid"
                      alt="guitarist jimi hendrix"
                    />
                  </a>
                  <div className="card-body">
                    <h3>JIMI HENDRIX</h3>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div
                  className="card"
                  style={{ width: "18rem", border: "none" }}
                >
                  <a href="">
                    <img
                      src="/guitarist.jpg"
                      className="card-img-top rounded-circle img-fluid"
                      alt="guitarist jimi hendrix"
                    />
                  </a>
                  <div className="card-body">
                    <h3>JIMI HENDRIX</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
