export default function TourSection() {
  return (
    <>
      <div className="tourSection">
        <h3 id="tour">TOUR DATES</h3>
        <p className="italic">
          Lorem ipsum dolor sit amet, consectetur adipisci elit
        </p>
        <p className="italic">Remember to book your tickets!</p>
        <ul className="list-group" style={{ width: "60%" }}>
          <li className="list-group-item d-flex align-items-center">
            September
            <span
              className="badge text-bg-danger rounded-pill"
              style={{ marginLeft: "1%" }}
            >
              SOLD OUT
            </span>
          </li>
          <li className="list-group-item d-flex align-items-center">
            October
            <span
              className="badge text-bg-danger rounded-pill"
              style={{ marginLeft: "1%" }}
            >
              SOLD OUT
            </span>
          </li>
          <li className="list-group-item d-flex justify-content-between align-items-center">
            November
            <span className="badge text-bg-secondary rounded-pill">2</span>
          </li>
        </ul>

        <div
          className="container-fluid"
          style={{ width: "60%", marginTop: "2%" }}
        >
          <div className="row">
            <div className="col-md-4">
              <div className="card" style={{ width: "15rem" }}>
                <img
                  src="/newyork.jpg"
                  className="card-img-top"
                  alt="newyork"
                />
                <div className="card-body" style={{ textAlign: "center" }}>
                  <h5>New York</h5>
                  <p style={{ fontStyle: "italic" }}>
                    <time dateTime="2015-11-27">Friday 27 November 2015</time>
                  </p>
                  <a href="#" className="btn btn-dark" id="ticket">
                    Buy tickets
                  </a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card" style={{ width: "15rem" }}>
                <img src="/tokyo.jpg" className="card-img-top" alt="tokyo" />
                <div className="card-body" style={{ textAlign: "center" }}>
                  <h5>Tokyo</h5>
                  <p style={{ fontStyle: "italic" }}>
                    <time dateTime="2015-11-27">Friday 27 November 2015</time>
                  </p>
                  <a href="#" className="btn btn-dark" id="ticket">
                    Buy tickets
                  </a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card" style={{ width: "15rem" }}>
                <img
                  src="/toronto.jpg"
                  className="card-img-top"
                  alt="toronto"
                />
                <div className="card-body" style={{ textAlign: "center" }}>
                  <h5>Toronto</h5>
                  <p style={{ fontStyle: "italic" }}>
                    <time dateTime="2015-11-27">Friday 27 November 2015</time>
                  </p>
                  <a href="#" className="btn btn-dark" id="ticket">
                    Buy tickets
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
