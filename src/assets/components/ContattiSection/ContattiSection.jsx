export default function ContattiSection() {
    return (
        <div className="contattiSection">
            <h2 id="contact">Contact</h2>
            <span style={{ fontStyle: 'italic' }}>We love our fans!</span>
            <br />
            <div className="container">
                <form className="row g-3">
                    <div className="col-md-4" style={{ textAlign: 'center' }}>
                        <span style={{ fontStyle: 'italic' }}>Fan? Drop a note.<br /></span>
                        <span><i className="bi bi-map"></i>Chicago, US <br /></span>
                        <span><i className="bi bi-telephone"></i>Phone: +00 151515151515<br /></span>
                        <span><i className="bi bi-mailbox"></i>Email: mail@mail.com</span>
                    </div>
                    <div className="col-md-8">
                        <div className="row">
                            <div className="col-md-4">
                                <input type="email" className="form-control" id="inputEmail4" placeholder="Email" />
                            </div>
                            <div className="col-md-4">
                                <input type="text" className="form-control" id="inputName4" placeholder="Name" />
                            </div>
                        </div>
                        <br />
                        <div className="form-floating col-md-8">
                            <textarea className="form-control" placeholder="Leave a comment here" id="comment"
                                style={{ height: '100px' }}></textarea>
                            <label htmlFor="comment" className="form-label">Comment</label>
                        </div>
                        <br />
                        <div className="col-12">
                            <button type="submit" className="btn btn-dark">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}