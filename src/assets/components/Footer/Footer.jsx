export default function Footer() {
  return (
      <div className="footerStyle">
        <a href="#">
          <i className="bi bi-chevron-compact-up" style={{color: "white"}}></i>
        </a>
        <span style={{color: "white"}}>
          Copryright 2024&copy; Alessandro Taormina
        </span>
      </div>
  );
}
