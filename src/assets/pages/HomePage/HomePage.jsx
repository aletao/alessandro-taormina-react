import Header from "../../components/Header/Header";
import Carousel from "../../components/Carousel/Carousel";
import BandSection from "../../components/BandSection/BandSection";
import TourSection from "../../components/TourSection/TourSection";
import ContattiSection from "../../components/ContattiSection/ContattiSection";
import Footer from "../../components/Footer/Footer";

export default function HomePage() {
  return (
    <>
      <Header></Header>
      <Carousel></Carousel>
      <BandSection></BandSection>
      <TourSection></TourSection>
      <ContattiSection></ContattiSection>
      <Footer></Footer>
    </>
  );
}
